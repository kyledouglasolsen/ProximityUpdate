﻿public interface ISpatialUpdate : ISpatialCallback
{
    void SpatialUpdate();
}