﻿using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnRandomEntities : MonoBehaviour
{
    [SerializeField] private float size = 100f;
    [SerializeField] private int count = 100;
    [SerializeField] private UnityCellEntity[] prefabs;
    private int totalSpawned = 0;

    private void OnEnable()
    {
        if (prefabs == null)
        {
            return;
        }

        for (var i = 0; i < count; ++i)
        {
            var position = transform.position + new Vector3(Random.Range(0f, size), 0f, Random.Range(0f, size));
            Instantiate(prefabs[i % prefabs.Length], position, Quaternion.identity);
        }

        totalSpawned += count;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        var offset = new Vector3(size / 2f, 0f, size / 2f);
        Gizmos.DrawWireCube(transform.position + offset, new Vector3(1f, 0f, 1f) * size);
    }
}