﻿using UnityEngine;

public class UnityGrid : MonoBehaviour
{
    [SerializeField] private int rows = 100, columns = 100, size = 1, neighborDistance = 1;

    public SpatialGrid SpatialGrid { get; private set; }

    private void OnValidate()
    {
        SpatialGrid = new SpatialGrid(rows, columns, size, neighborDistance);
    }

    private void OnDrawGizmos()
    {
        for (var i = 0; i < SpatialGrid.Cells.Length; ++i)
        {
            Gizmos.color = SpatialGrid.Cells[i].Contents.Count < 1 ? new Color(1f, 1f, 1f, 0.5f) : new Color(1f, 0f, 0f, 1f);
            SpatialGrid.Cells[i].DrawGizmo();
        }
    }
}