﻿using UnityEngine;

public class UnityCellEntityGizmos : MonoBehaviour
{
    private UnityCellEntity myEntity;
    
#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        if(myEntity == null)
        {
            myEntity = GetComponent<UnityCellEntity>();
        }
        
        if (myEntity?.Cell?.Neighbors == null)
        {
            return;
        }

        foreach (var entity in myEntity?.Cell.Visible)
        {
            Gizmos.DrawLine(myEntity.Position, entity.Position);
        }
    }
#endif
}