﻿using System;
using UnityEngine;

public class UnityCellEntity : MonoBehaviour
{
    public static event Action<UnityCellEntity> AddedEntity = entity => { };
    public static event Action<UnityCellEntity> RemovedEntity = entity => { };

    public int Id { get; private set; }
    public bool IsViewer => isViewer;
    public bool IsStatic => isStatic;
    public bool IsActive { get; private set; }
    public SpatialGridCell Cell { get; private set; }
    public Vector3 Position => transform.position;

    [SerializeField] private bool isViewer = false, isStatic = false;
    private ISpatialCallback[] callbacks;

    private void Awake()
    {
        callbacks = GetComponents<ISpatialCallback>();
    }

    private void OnEnable()
    {
        AddedEntity(this);
    }

    private void OnDisable()
    {
        RemovedEntity(this);
    }

    public void SetId(int id)
    {
        Id = id;
    }

    public void SetCell(SpatialGridCell newCell)
    {
        Cell = newCell;
    }

    public void SpatialEnable(float sleepDeltaTime)
    {
        IsActive = true;

        for (var i = 0; i < callbacks.Length; ++i)
        {
            (callbacks[i] as ISpatialEnable)?.SpatialEnable(sleepDeltaTime);
        }
    }

    public void SpatialDisable()
    {
        IsActive = false;

        for (var i = 0; i < callbacks.Length; ++i)
        {
            (callbacks[i] as ISpatialDisable)?.SpatialDisable();
        }
    }

    public void SpatialUpdate()
    {
        for (var i = 0; i < callbacks.Length; ++i)
        {
            (callbacks[i] as ISpatialUpdate)?.SpatialUpdate();
        }
    }
}