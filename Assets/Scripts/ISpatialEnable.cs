﻿public interface ISpatialEnable : ISpatialCallback
{
    void SpatialEnable(float inactiveSeconds);
}