﻿using UnityEngine;

public class RandomMove : MonoBehaviour, ISpatialEnable, ISpatialDisable, ISpatialUpdate
{
    [SerializeField] private float speed = 1f, maxMoveDistance = 50f;
    private Vector3 startPosition, targetPosition;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void NewRandomPosition()
    {
        var random = Random.insideUnitSphere * maxMoveDistance;
        random.y = 0f;
        targetPosition = startPosition + random;
    }

    public void SpatialEnable(float inactiveSeconds)
    {
        NewRandomPosition();

        InvokeRepeating(nameof(NewRandomPosition), 1f, Random.Range(0.5f, 2f));
    }

    public void SpatialDisable()
    {
        CancelInvoke(nameof(NewRandomPosition));
    }

    public void SpatialUpdate()
    {
        var position = transform.position;
        position = Vector3.MoveTowards(position, targetPosition, speed * Time.deltaTime);
        transform.position = position;
    }
}