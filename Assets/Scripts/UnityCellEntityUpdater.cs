﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

public class UnityCellEntityUpdater : MonoBehaviour
{
    private static readonly HashSet<UnityCellEntity> EntitiesToAdd = new HashSet<UnityCellEntity>();
    private static readonly HashSet<UnityCellEntity> EntitiesToRemove = new HashSet<UnityCellEntity>();
    private static readonly Queue<int> AvailableIndexes = new Queue<int>();
    private static TransformAccessArray transformAccess;
    private readonly List<UnityCellEntity> entities = new List<UnityCellEntity>(1000);
    private Coroutine slowUpdate;
    private NativeList<int> newIds;
    private SpatialGrid partition;
    private JobHandle updateCellsJob;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
        if (transformAccess.isCreated)
        {
            transformAccess.Dispose();
        }

        transformAccess = new TransformAccessArray(500);

        UnityCellEntity.AddedEntity += entity =>
        {
            EntitiesToAdd.Add(entity);
            EntitiesToRemove.Remove(entity);
        };

        UnityCellEntity.RemovedEntity += entity =>
        {
            EntitiesToRemove.Add(entity);
            EntitiesToAdd.Remove(entity);
        };
    }

    private void Awake()
    {
        partition = GetComponent<UnityGrid>().SpatialGrid;
        newIds = new NativeList<int>(10000, Allocator.Persistent);

        slowUpdate = StartCoroutine(SlowUpdate());
    }

    private void OnDestroy()
    {
        if (slowUpdate != null)
        {
            StopCoroutine(slowUpdate);
        }

        if (newIds.IsCreated)
        {
            newIds.Dispose();
        }

        if (transformAccess.isCreated)
        {
            transformAccess.Dispose();
        }
    }

    private void Update()
    {
        for (var i = 0; i < partition.Cells.Length; ++i)
        {
            if (!partition.Cells[i].Active)
            {
                continue;
            }

            partition.Cells[i].Update();
        }
    }

    private IEnumerator SlowUpdate()
    {
        while (Application.isPlaying)
        {
            HandleNewEntities();
            yield return null;
            Schedule();
            yield return null;
            Complete();
            yield return null;
            Process();
            yield return null;
        }
    }

    private void Schedule()
    {
        var job = new UpdateCellsJob(partition, newIds);
        updateCellsJob = job.Schedule(transformAccess);
    }

    private void Complete()
    {
        updateCellsJob.Complete();
    }

    private void Process()
    {
        for (var i = 0; i < entities.Count; ++i)
        {
            if (entities[i] == null)
            {
                continue;
            }

            var currentCell = entities[i].Cell;
            var currentId = currentCell?.Id ?? -1;

            if (currentId != newIds[i])
            {
                var newCell = partition.GetCell(newIds[i]);
                newCell.AddEntity(entities[i]);
                currentCell?.RemoveEntity(entities[i]);
            }
        }
    }

    private void AddEntity(UnityCellEntity entity)
    {
        if (entity.IsStatic)
        {
            partition.GetCell(entity.Position).AddEntity(entity);
            entity.SetId(-1);
            return;
        }

        int index;

        if (AvailableIndexes.Count > 0)
        {
            index = AvailableIndexes.Dequeue();
            entities[index] = entity;
            transformAccess[index] = entity.transform;
            newIds[index] = -1;
        }
        else
        {
            index = entities.Count;
            entities.Add(entity);
            transformAccess.Add(entity.transform);
            newIds.Add(-1);
        }

        entity.SetId(index);
    }

    private void RemoveEntity(UnityCellEntity entity)
    {
        if (entity.IsStatic)
        {
            entity.Cell.RemoveEntity(entity);
            return;
        }

        AvailableIndexes.Enqueue(entity.Id);

        entities[entity.Id] = null;

        if (transformAccess.isCreated)
        {
            transformAccess[entity.Id] = null;
        }

        if (newIds.IsCreated)
        {
            newIds[entity.Id] = -1;
        }

        entity.Cell.RemoveEntity(entity);
    }

    private void HandleNewEntities()
    {
        foreach (var entity in EntitiesToAdd)
        {
            AddEntity(entity);
        }

        foreach (var entity in EntitiesToRemove)
        {
            RemoveEntity(entity);
        }

        EntitiesToAdd.Clear();
        EntitiesToRemove.Clear();
    }

    private struct UpdateCellsJob : IJobParallelForTransform
    {
        [ReadOnly] private SpatialGrid partition;
        [WriteOnly] private NativeArray<int> newIds;

        public UpdateCellsJob(SpatialGrid partition, NativeArray<int> newIds)
        {
            this.partition = partition;
            this.newIds = newIds;
        }

        public void Execute(int i, TransformAccess transform)
        {
            newIds[i] = partition.GetCell(transform.position).Id;
        }
    }
}