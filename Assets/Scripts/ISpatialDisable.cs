﻿public interface ISpatialDisable : ISpatialCallback
{
    void SpatialDisable();
}