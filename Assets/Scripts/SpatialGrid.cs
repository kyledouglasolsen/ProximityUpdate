﻿using System.Collections.Generic;
using UnityEngine;

public struct SpatialGrid
{
    public readonly int Rows, Columns, Size;
    public readonly SpatialGridCell[] Cells;

    public SpatialGrid(int rows, int columns, int size, int neighborDistance)
    {
        Rows = rows;
        Columns = columns;
        Size = size;

        Cells = new SpatialGridCell[rows * columns];

        for (var x = 0; x < rows; ++x)
        for (var y = 0; y < columns; ++y)
        {
            var index = x * rows + y;
            var position = new Vector3(x * Size, 0f, y * Size);
            Cells[index] = new SpatialGridCell(index, position, ref this);
        }

        for (var x = 0; x < rows; ++x)
        for (var y = 0; y < columns; ++y)
        {
            var index = x * rows + y;

            for (var nx = Mathf.Clamp(x - neighborDistance, 0, rows); nx < Mathf.Clamp(x + neighborDistance + 1, 0, rows); ++nx)
            for (var ny = Mathf.Clamp(y - neighborDistance, 0, columns); ny < Mathf.Clamp(y + neighborDistance + 1, 0, columns); ++ny)
            {
                var neighborIndex = nx * rows + ny;

                if (index == neighborIndex)
                {
                    continue;
                }

                Cells[index].AddNeighbor(ref Cells[neighborIndex]);
            }
        }
    }

    public SpatialGridCell GetCell(int id)
    {
        if (id < 0 || id >= Cells.Length)
        {
            return default(SpatialGridCell);
        }

        return Cells[id];
    }

    public SpatialGridCell GetCell(Vector3 position)
    {
        var xIndex = (int)(position.x / Size);
        var yIndex = (int)(position.z / Size);

        if (xIndex >= Rows)
            xIndex = Rows - 1;

        if (xIndex < 0)
            xIndex = 0;

        if (yIndex >= Columns)
            yIndex = Columns - 1;

        if (yIndex < 0)
            yIndex = 0;

        return Cells[xIndex * Rows + yIndex];
    }
}

public class SpatialGridCell
{
    public int Id => initialized ? id : -1;
    public bool Active => visibleByCount > 0;
    public readonly Vector3 Position;
    public readonly HashSet<UnityCellEntity> Contents;
    public readonly HashSet<SpatialGridCell> Neighbors;

    private readonly bool initialized;
    private readonly int id;
    private readonly SpatialGrid spatialGrid;
    private int visibleByCount;
    private float lastSleepTime;

    public SpatialGridCell(int id, Vector3 position, ref SpatialGrid spatialGrid)
    {
        this.id = id;
        this.spatialGrid = spatialGrid;
        Contents = new HashSet<UnityCellEntity>();
        Neighbors = new HashSet<SpatialGridCell>();

        var offset = spatialGrid.Size / 2f;
        Position = new Vector3(position.x + offset, 0f, position.z + offset);
        visibleByCount = 0;
        lastSleepTime = Time.time;
        initialized = true;
    }

    public void Update()
    {
        foreach (var entity in Contents)
        {
            if (entity == null)
            {
                continue;
            }

            entity.SpatialUpdate();
        }
    }

    public bool AddEntity(UnityCellEntity entity)
    {
        var added = initialized && Contents.Add(entity);

        if (added)
        {
            if (Active && (!entity.IsActive || entity.Cell == null))
            {
                entity.SpatialEnable(0f);
            }
            else if (!Active && (entity.IsActive || entity.Cell == null))
            {
                entity.SpatialDisable();
            }

            entity.SetCell(this);

            if (entity.IsViewer)
            {
                AddVisibleBy();

                foreach (var neighbor in Neighbors)
                {
                    neighbor.AddVisibleBy();
                }
            }
        }

        return added;
    }

    public bool RemoveEntity(UnityCellEntity entity)
    {
        var removed = initialized && Contents.Remove(entity);

        if (removed)
        {
            if (entity.IsViewer)
            {
                RemoveVisibleBy();

                foreach (var neighbor in Neighbors)
                {
                    neighbor.RemoveVisibleBy();
                }
            }
        }

        return removed;
    }

    public bool AddNeighbor(ref SpatialGridCell cell)
    {
        return initialized && Neighbors.Add(cell);
    }

    public bool RemoveNeighbor(ref SpatialGridCell cell)
    {
        return initialized && Neighbors.Remove(cell);
    }

    public void AddVisibleBy()
    {
        ++visibleByCount;

        if (visibleByCount == 1)
        {
            var sleepDeltaTime = Time.time - lastSleepTime;

            foreach (var entity in Contents)
            {
                entity.SpatialEnable(sleepDeltaTime);
            }
        }
    }

    public void RemoveVisibleBy()
    {
        --visibleByCount;

        if (visibleByCount == 0)
        {
            lastSleepTime = Time.time;

            foreach (var entity in Contents)
            {
                entity.SpatialDisable();
            }
        }
    }

    public IEnumerable<UnityCellEntity> Visible
    {
        get
        {
            foreach (var entity in Contents)
            {
                yield return entity;
            }

            foreach (var cell in Neighbors)
            {
                foreach (var entity in cell.Contents)
                {
                    yield return entity;
                }
            }
        }
    }

#if UNITY_EDITOR
    public void DrawGizmo()
    {
        var insetSize = spatialGrid.Size * 0.995f;
        var size = new Vector3(insetSize, 0f, insetSize);

        Gizmos.DrawWireCube(Position, size);

//        if (visibleByCount > 0)
//        {
//            UnityEditor.Handles.BeginGUI();
//            var restoreColor = UnityEditor.Handles.color;
//            UnityEditor.Handles.color = Color.white;
//
//            var view = UnityEditor.SceneView.currentDrawingSceneView;
//            var screenPos = view.camera.WorldToScreenPoint(Position);
//
//            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
//            {
//                UnityEditor.Handles.color = restoreColor;
//                UnityEditor.Handles.EndGUI();
//                return;
//            }
//
//            var text = visibleByCount.ToString();
//            UnityEditor.Handles.Label(Position, text);
//
//            UnityEditor.Handles.color = restoreColor;
//            UnityEditor.Handles.EndGUI();
//        }
    }
#endif
}