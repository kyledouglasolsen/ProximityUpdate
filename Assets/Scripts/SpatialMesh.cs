﻿using UnityEngine;

public class SpatialMesh : MonoBehaviour, ISpatialEnable, ISpatialDisable
{
    private Renderer[] renderers;

    private void Awake()
    {
        renderers = GetComponentsInChildren<Renderer>();
    }

    public void SpatialEnable(float inactiveSeconds)
    {
        for (var i = 0; i < renderers.Length; ++i)
        {
            renderers[i].enabled = true;
        }
    }

    public void SpatialDisable()
    {
        for (var i = 0; i < renderers.Length; ++i)
        {
            renderers[i].enabled = false;
        }
    }
}